from django.shortcuts import render, redirect
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from todos.models import TodoList, TodoItem
from django.urls import reverse_lazy


class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
    # fields = ["task", "due_date", "is_completed", "list"]
    # context_object_name = "todo_list_detail"

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     return context


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/update.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    fields = ["name"]
    success_url = reverse_lazy("todo_list_list")


# DOESNT WORK HERE
# def get_success_url(self):
#     return reverse_lazy("todo_list_list", args=[self.object.id])


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "items/create.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        # print("HEREEEEEEEEEEEEEE", self.object.__dict__)
        return reverse_lazy("todo_list_detail", args=[self.object.list_id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "items/update.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        # print("HEREEEEEEEEEEEEEE", self.object.__dict__)
        return reverse_lazy("todo_list_detail", args=[self.object.list_id])

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context["TodoList"] = TodoItem.list
    #     return context
