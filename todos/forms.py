from django.forms import CharField, ModelForm

from todos.models import TodoList


class TodoForm(ModelForm):
    name = CharField(max_length=100)

    class Meta:
        model = TodoList
        fields = ["name"]
